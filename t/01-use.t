#!/usr/bin/perl
# $Id$
use strict;
use warnings;
use Test::More tests => 2;
use lib 'lib';

BEGIN { use_ok('FrameMaker::MifTree') };

is($FrameMaker::MifTree::VERSION, 0.075);

__END__

#!/bin/sh

if [ x`pwd | awk -F/ '{ print $NF }'` == xMifTreeWorkFiles ]
then
  cd ..
fi

if [ x`pwd | awk -F/ '{ print $NF }'` == xFrameMaker-MifTree ]
then

  svn-co IO-Tokenized-Scalar
  mkdir -p lib/IO/Tokenized
  cp IO-Tokenized-Scalar/lib/IO/Tokenized/Scalar.pm lib/IO/Tokenized/
  rm -fr IO-Tokenized-Scalar

else

  echo Aanroep vanuit verkeerde directory...

fi

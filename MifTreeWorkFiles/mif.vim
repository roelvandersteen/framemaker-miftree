" Vim syntax file
" $Id$
" Language:        MIF (Maker Interchange Format)
" Filenames:       *.mif

if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

" Everything that follows a "<" is considered a statement.
"syn region  mifStatement        start=/</ms=e+1 end=/\s/me=s-1
syn keyword mifStatement	AFrame AFrames ATbl AcrobatElement
syn keyword mifStatement	AcrobatElements AlsoInsert AnchorAlign
syn keyword mifStatement	AnchorBeside Angle Arc ArcDTheta ArcRect
syn keyword mifStatement	ArcTheta ArrowStyle AsianSpace AttrName
syn keyword mifStatement	AttrValue Attribute AttributeDisplay Attributes
syn keyword mifStatement	AutoNumSeries BAttrEditor BAttributeDisplay
syn keyword mifStatement	BCustomElementList BDisplayText
syn keyword mifStatement	BElementCatalogScope BFCLMaximums BFCLMinimums
syn keyword mifStatement	BFNoteComputeMethod BFNoteLabels BFNoteNumStyle
syn keyword mifStatement	BFNoteRestart BFNoteStartNum BLOffset BRect
syn keyword mifStatement	BSGMLAppName BSeparateInclusions BStructAppName
syn keyword mifStatement	BTblFNoteComputeMethod BTblFNoteLabels
syn keyword mifStatement	BTblFNoteNumStyle BUseInitStructure
syn keyword mifStatement	BUseInitialStructure BViewOnly BViewOnlyNoOp
syn keyword mifStatement	BViewOnlyPopup BViewOnlyWinBorders
syn keyword mifStatement	BViewOnlyWinMenubar BWindowRect BXmlDocType
syn keyword mifStatement	BXmlEncoding BXmlFileEncoding BXmlStandAlone
syn keyword mifStatement	BXmlStyleSheet BXmlUseBOM BXmlVersion
syn keyword mifStatement	BXmlWellFormed BaseAngle BaseCharWithRubi
syn keyword mifStatement	BaseCharWithSuper BegParentheses BitMapDpi Book
syn keyword mifStatement	BookComponent BookElements BookFileInfo
syn keyword mifStatement	BookSettings BookUpdateReferences BookXRef
syn keyword mifStatement	CColor CSeparation CState CStyle CTag Cell
syn keyword mifStatement	CellAffectsColumnWidthA CellAngle CellBRuling
syn keyword mifStatement	CellColor CellColumns CellContent CellFill
syn keyword mifStatement	CellLRuling CellRRuling CellRows CellSeparation
syn keyword mifStatement	CellTRuling CenteredPunct
syn keyword mifStatement	ChapterNumComputeMethod ChapterNumStart
syn keyword mifStatement	ChapterNumStyle ChapterNumText Char CharClass
syn keyword mifStatement	CharUnits Collapsed Color ColorAttribute
syn keyword mifStatement	ColorBlack ColorCatalog ColorCyan
syn keyword mifStatement	ColorLibraryFamilyName ColorLibraryInkName
syn keyword mifStatement	ColorMagenta ColorOverprint ColorTag ColorTint
syn keyword mifStatement	ColorTintBaseColor ColorTintPercentage
syn keyword mifStatement	ColorYellow ColumnGap Columns
syn keyword mifStatement	CombinedFontAllowBaseFamilyBoldedAndObliqued
syn keyword mifStatement	CombinedFontBaseEncoding CombinedFontBaseFamily
syn keyword mifStatement	CombinedFontCatalog CombinedFontDefn
syn keyword mifStatement	CombinedFontName CombinedFontWesternFamily
syn keyword mifStatement	CombinedFontWesternShift
syn keyword mifStatement	CombinedFontWesternSize Comment Condition
syn keyword mifStatement	ConditionCatalog Conditional ContPageNum
syn keyword mifStatement	Context ContextFormatRule ContextLabel
syn keyword mifStatement	CountElement CountElements Cropped
syn keyword mifStatement	DAcrobatBookmarksIncludeTagNames
syn keyword mifStatement	DAcrobatEnableArticleThreads
syn keyword mifStatement	DAcrobatParagraphBookmarks DApplyFormatRules
syn keyword mifStatement	DAttrEditor DAttributeDisplay DAutoChBars
syn keyword mifStatement	DBookElementHierarchy DBordersOn DChBarColor
syn keyword mifStatement	DChBarGap DChBarPosition DChBarWidth DColumnGap
syn keyword mifStatement	DColumns DCurrentView DCustomElementList
syn keyword mifStatement	DDisplayOverrides DElementBordersOn
syn keyword mifStatement	DElementCatalogScope DElementTags
syn keyword mifStatement	DElementTagsOn DExclusions DFCLMaximums
syn keyword mifStatement	DFCLMinimums DFNoteAnchorPos DFNoteAnchorPrefix
syn keyword mifStatement	DFNoteAnchorSuffix DFNoteComputeMethod
syn keyword mifStatement	DFNoteLabels DFNoteMaxH DFNoteNumStyle
syn keyword mifStatement	DFNoteNumberPos DFNoteNumberPrefix
syn keyword mifStatement	DFNoteNumberSuffix DFNoteRestart DFNoteTag
syn keyword mifStatement	DFluid DFluidSideheads DFrozenPages DFullRulers
syn keyword mifStatement	DGenerateAcrobatInfo DGraphicsOff DGridOn
syn keyword mifStatement	DInclusions DLOut DLSource DLanguage
syn keyword mifStatement	DLinebreakChars DLinkBoundariesOn DMagicMarker
syn keyword mifStatement	DMargins DMathAlphaCharFontFamily DMathCatalog
syn keyword mifStatement	DMathFunctions DMathGreek DMathGreekOverrides
syn keyword mifStatement	DMathLargeHoriz DMathLargeIntegral
syn keyword mifStatement	DMathLargeLevel1 DMathLargeLevel2
syn keyword mifStatement	DMathLargeLevel3 DMathLargeSigma DMathLargeVert
syn keyword mifStatement	DMathMediumHoriz DMathMediumIntegral
syn keyword mifStatement	DMathMediumLevel1 DMathMediumLevel2
syn keyword mifStatement	DMathMediumLevel3 DMathMediumSigma
syn keyword mifStatement	DMathMediumVert DMathNew DMathNewType
syn keyword mifStatement	DMathNumbers DMathOpName DMathOpOverrides
syn keyword mifStatement	DMathOpPositionA DMathOpPositionB
syn keyword mifStatement	DMathOpPositionC DMathOpTLineOverride
syn keyword mifStatement	DMathShowCustom DMathSmallHoriz
syn keyword mifStatement	DMathSmallIntegral DMathSmallLevel1
syn keyword mifStatement	DMathSmallLevel2 DMathSmallLevel3
syn keyword mifStatement	DMathSmallSigma DMathSmallVert DMathStrings
syn keyword mifStatement	DMathVariables DMenuBar
syn keyword mifStatement	DNarrowRubiSpaceForJapanese
syn keyword mifStatement	DNarrowRubiSpaceForOther DNextUnique
syn keyword mifStatement	DNoPrintSepColor DPDFAllNamedDestinations
syn keyword mifStatement	DPDFAllPages DPDFBookmarks DPDFConvertCMYK
syn keyword mifStatement	DPDFDestsMarked DPDFEndPage DPDFFit
syn keyword mifStatement	DPDFJobOptions DPDFOpenBookmarkLevel
syn keyword mifStatement	DPDFOpenFit DPDFOpenPage DPDFOpenZoom
syn keyword mifStatement	DPDFPageHeight DPDFPageSizeSet DPDFPageWidth
syn keyword mifStatement	DPDFRegMarks DPDFSaveSeparate DPDFStartPage
syn keyword mifStatement	DPDFStructure DPDFStructureDefined DPDFZoom
syn keyword mifStatement	DPageGrid DPageNumStyle DPagePointStyle
syn keyword mifStatement	DPageRounding DPageScrolling DPageSize DParity
syn keyword mifStatement	DPrintProcessColor DPrintSeparations
syn keyword mifStatement	DPrintSkipBlankPages DPunctuationChars
syn keyword mifStatement	DRubiAlignAtBounds DRubiAlignAtLineBounds
syn keyword mifStatement	DRubiFixedSize DRubiOverhang DRubiSize
syn keyword mifStatement	DRulersOn DSGMLAppName DSeparateInclusions
syn keyword mifStatement	DShowAllConditions DSmallCapsSize
syn keyword mifStatement	DSmallCapsStretch DSmartQuotesOn DSmartSpacesOn
syn keyword mifStatement	DSnapGrid DSnapRotation DStartPage
syn keyword mifStatement	DStructAppName DSubscriptShift DSubscriptSize
syn keyword mifStatement	DSubscriptStretch DSuperscriptShift
syn keyword mifStatement	DSuperscriptSize DSuperscriptStretch DSymbolsOn
syn keyword mifStatement	DTblFNoteAnchorPos DTblFNoteAnchorPrefix
syn keyword mifStatement	DTblFNoteAnchorSuffix DTblFNoteLabels
syn keyword mifStatement	DTblFNoteNumStyle DTblFNoteNumberPos
syn keyword mifStatement	DTblFNoteNumberPrefix DTblFNoteNumberSuffix
syn keyword mifStatement	DTblFNoteTag DTrapwiseCompatibility DTwoSides
syn keyword mifStatement	DUpdateTextInsetsOnOpen DUpdateXRefsOnOpen
syn keyword mifStatement	DUseInitStructure DUseInitialStructure
syn keyword mifStatement	DViewOnly DViewOnlyNoOp DViewOnlySelect
syn keyword mifStatement	DViewOnlyWinBorders DViewOnlyWinMenubar
syn keyword mifStatement	DViewOnlyWinPalette DViewOnlyWinPopup
syn keyword mifStatement	DViewOnlyXRef DViewRect DViewScale DVoMenuBar
syn keyword mifStatement	DWideRubiSpaceForJapanese
syn keyword mifStatement	DWideRubiSpaceForOther DWindowRect DXmlDocType
syn keyword mifStatement	DXmlEncoding DXmlFileEncoding DXmlPublicId
syn keyword mifStatement	DXmlStandAlone DXmlStyleSheet DXmlSystemId
syn keyword mifStatement	DXmlUseBOM DXmlVersion DXmlWellFormed
syn keyword mifStatement	DashSegment DashedPattern DashedStyle DataLink
syn keyword mifStatement	DataLinkEnd DefaultApply DefaultDerive
syn keyword mifStatement	DefaultPrint DeriveLinks DeriveTag DeriveType
syn keyword mifStatement	Dictionary DisplayText DocFileInfo Document
syn keyword mifStatement	EComponent EDAlsoInsert EDAttrChoice
syn keyword mifStatement	EDAttrChoices EDAttrDef EDAttrDefValue
syn keyword mifStatement	EDAttrDefValues EDAttrDefinitions EDAttrHidden
syn keyword mifStatement	EDAttrName EDAttrRange EDAttrReadOnly
syn keyword mifStatement	EDAttrRequired EDAttrType EDComments
syn keyword mifStatement	EDEndElementRules EDExclusions EDGeneralRule
syn keyword mifStatement	EDInclusions EDInitialTablePattern EDObject
syn keyword mifStatement	EDObjectFormatRules EDPgfFormat EDPrefixRules
syn keyword mifStatement	EDRangeEnd EDRangeStart EDStartElementRules
syn keyword mifStatement	EDSuffixRules EDTag EDTextFormatRules
syn keyword mifStatement	EDValidHighestLevel ENamespace ENamespacePath
syn keyword mifStatement	ENamespacePrefix ETag ETextSnippet Element
syn keyword mifStatement	ElementBegin ElementContext ElementDef
syn keyword mifStatement	ElementDefCatalog ElementEnd ElementPrefix
syn keyword mifStatement	ElementReferenced ElementSuffix Ellipse Else
syn keyword mifStatement	ElseIf EndParentheses EqualizeWidths Exclusion
syn keyword mifStatement	ExtraSpaceTable FAngle FBold FCase FChangeBar
syn keyword mifStatement	FColor FCombinedFontName FDW FDWChange FDX FDY
syn keyword mifStatement	FEncoding FFamily FItalic FLanguage FLocked
syn keyword mifStatement	FNote FNote FNoteStartNum FOutline FOverline
syn keyword mifStatement	FPairKern FPlain FPlatformName FPosition
syn keyword mifStatement	FPostScriptName FSeparation FShadow FSize
syn keyword mifStatement	FSizeChange FStretch FStretchChange FStrike
syn keyword mifStatement	FTag FTsume FUnderline FUnderlining FVar
syn keyword mifStatement	FWeight FWesternPlatformName
syn keyword mifStatement	FWesternPostScriptName FclPgfCatalogRef FclTag
syn keyword mifStatement	FileName FileNameSuffix Fill FlipLR Float
syn keyword mifStatement	FlowTag FmtChangeList FmtChangeListCatalog
syn keyword mifStatement	FmtChangeListTag Font FontCatalog FooterC
syn keyword mifStatement	FooterL FooterR FormatTag Frame FrameType Group
syn keyword mifStatement	GroupID HFFont HFMargins HeadCap HeadType
syn keyword mifStatement	HeaderC HeaderL HeaderR Hiragana HyphenMaxLines
syn keyword mifStatement	HyphenMinPrefix HyphenMinSuffix HyphenMinWord
syn keyword mifStatement	ID If ImportHint ImportObEditor ImportObFile
syn keyword mifStatement	ImportObFileDI ImportObFixedSize
syn keyword mifStatement	ImportObUpdater ImportObject InCondition
syn keyword mifStatement	Inclusion InitialAutoNums IsTextRange KLanguage
syn keyword mifStatement	Key Kumihan KumihanCatalog Length Level
syn keyword mifStatement	LevelFormatRule LineBeginEnd LineBreakTable
syn keyword mifStatement	MCurrPage MIFEncoding MIFFile MText MText MType
syn keyword mifStatement	MTypeName MacEdition Marker MarkerTypeCatalog
syn keyword mifStatement	Math MathAlignment MathFullForm MathLineBreak
syn keyword mifStatement	MathOrigin MathSize MoveTabs NSOffset
syn keyword mifStatement	NativeOrigin NextElement NoLineBeginChar
syn keyword mifStatement	NonSeparableChar Notes NumCounter NumPages
syn keyword mifStatement	NumPoints NumRunaroundPolygons NumSegments
syn keyword mifStatement	Numeral OKWord ObColor ObTint ObjectAttribute
syn keyword mifStatement	OneLinePerRec Others Overprint PDFBookInfo
syn keyword mifStatement	PDFDocInfo Page PageAngle PageBackground
syn keyword mifStatement	PageNum PageNumStart PageNumStyle PageNumbering
syn keyword mifStatement	PageOrientation PageSize PageTag PageType Para
syn keyword mifStatement	ParaLine ParenBegWariChu ParenBeginWariChu
syn keyword mifStatement	ParenEndWariChu Pen PenWidth PeriodComma Pgf
syn keyword mifStatement	PgfAcrobatLevel PgfAlignment PgfAutoNum
syn keyword mifStatement	PgfBlockSize PgfBotSepAtIndent PgfBotSepOffset
syn keyword mifStatement	PgfBotSeparator PgfCatalog PgfCellAlignment
syn keyword mifStatement	PgfCellBMargin PgfCellBMarginChange
syn keyword mifStatement	PgfCellBMarginFixed PgfCellLMargin
syn keyword mifStatement	PgfCellLMarginChange PgfCellLMarginFixed
syn keyword mifStatement	PgfCellMargins PgfCellRMargin
syn keyword mifStatement	PgfCellRMarginChange PgfCellRMarginFixed
syn keyword mifStatement	PgfCellTMargin PgfCellTMarginChange
syn keyword mifStatement	PgfCellTMarginFixed PgfCondFullPgf PgfEndCond
syn keyword mifStatement	PgfFIndent PgfFIndentChange PgfFIndentOffset
syn keyword mifStatement	PgfFIndentRelative PgfFont PgfHyphenate
syn keyword mifStatement	PgfLIndent PgfLIndentChange PgfLanguage
syn keyword mifStatement	PgfLeading PgfLeadingChange PgfLetterSpace
syn keyword mifStatement	PgfLineSpacing PgfLineSpacingFixed PgfLocked
syn keyword mifStatement	PgfMaxJLetterSpace PgfMaxJRomanLetterSpace
syn keyword mifStatement	PgfMaxWordSpace PgfMinJLetterSpace
syn keyword mifStatement	PgfMinJRomanLetterSpace PgfMinWordSpace
syn keyword mifStatement	PgfNextTag PgfNumAtEnd PgfNumFormat
syn keyword mifStatement	PgfNumString PgfNumTabs PgfNumberFont
syn keyword mifStatement	PgfNumbering PgfOptJLetterSpace
syn keyword mifStatement	PgfOptJRomanLetterSpace PgfOptWordSpace
syn keyword mifStatement	PgfPDFStructureLevel PgfPlacement
syn keyword mifStatement	PgfPlacementStyle PgfRIndent PgfRIndentChange
syn keyword mifStatement	PgfReferenced PgfRunInDefaultPunct PgfSpAfter
syn keyword mifStatement	PgfSpAfterChange PgfSpBefore PgfSpBeforeChange
syn keyword mifStatement	PgfTag PgfTopSepAtIndent PgfTopSepOffset
syn keyword mifStatement	PgfTopSeparator PgfUseNextTag PgfWithNext
syn keyword mifStatement	PgfWithPrev PgfYakumonoType Point PolyLine
syn keyword mifStatement	Polygon PrecedingSymbol PrevElement
syn keyword mifStatement	QuestionBang Radius ReRotateAngle Rectangle
syn keyword mifStatement	RomanChar RomanSpace RoundRect Row RowHeight
syn keyword mifStatement	RowMaxHeight RowMinHeight RowPlacement
syn keyword mifStatement	RowWithNext RowWithPrev Ruling RulingCatalog
syn keyword mifStatement	RulingColor RulingGap RulingLines RulingPen
syn keyword mifStatement	RulingPenWidth RulingSeparation RulingTag
syn keyword mifStatement	RunaroundGap RunaroundPolygon RunaroundPolygons
syn keyword mifStatement	RunaroundType ScaleFactor ScaleHead Separation
syn keyword mifStatement	Series ShapeRect Smoothed Spare1 Spare2 Spare3
syn keyword mifStatement	Spare4 Spare5 SpclHyphenation SpecialCase
syn keyword mifStatement	SpreadTable SqueezeHorizontal SqueezeTable
syn keyword mifStatement	SqueezeVertical StartPageSide StopCountingAt
syn keyword mifStatement	String SucceedingSymbol TFAutoConnect TFFeather
syn keyword mifStatement	TFLineSpacing TFMaxInterLine TFMaxInterPgf
syn keyword mifStatement	TFMinHangHeight TFPostScript TFSideheads
syn keyword mifStatement	TFSynchronized TFTag TLAlignment TLLanguage
syn keyword mifStatement	TLOrigin TRColumnBalance TRColumnGap TRNext
syn keyword mifStatement	TRNumColumns TRSideheadGap TRSideheadPlacement
syn keyword mifStatement	TRSideheadWidth TSDecimalChar TSLeaderStr
syn keyword mifStatement	TSType TSX TSXRelative TabStop Tag TailCap Tbl
syn keyword mifStatement	TblAlignment TblAltShadePeriod TblBRuling
syn keyword mifStatement	TblBlockSize TblBody TblBodyColor TblBodyFill
syn keyword mifStatement	TblBodyRowRuling TblBodySeparation TblCatalog
syn keyword mifStatement	TblCellMargins TblColumn TblColumnBody
syn keyword mifStatement	TblColumnF TblColumnH TblColumnNum
syn keyword mifStatement	TblColumnRuling TblColumnWidth TblColumnWidthA
syn keyword mifStatement	TblColumnWidthP TblF TblFormat TblH TblHFColor
syn keyword mifStatement	TblHFFill TblHFRowRuling TblHFSeparation TblID
syn keyword mifStatement	TblInitNumBodyRows TblInitNumColumns
syn keyword mifStatement	TblInitNumFRows TblInitNumHRows TblLIndent
syn keyword mifStatement	TblLRuling TblLastBRuling TblLocked
syn keyword mifStatement	TblNumByColumn TblNumColumns TblPlacement
syn keyword mifStatement	TblRIndent TblRRuling TblRulingPeriod
syn keyword mifStatement	TblSeparatorRuling TblShadeByColumn
syn keyword mifStatement	TblShadePeriod TblSpAfter TblSpBefore
syn keyword mifStatement	TblTRuling TblTag TblTitle TblTitleContent
syn keyword mifStatement	TblTitleGap TblTitlePgf1 TblTitlePlacement
syn keyword mifStatement	TblWidth TblXColor TblXColumnNum
syn keyword mifStatement	TblXColumnRuling TblXFill TblXRowRuling
syn keyword mifStatement	TblXSeparation Tbls TextFlow TextInset
syn keyword mifStatement	TextInsetEnd TextLine TextRect TextRectID
syn keyword mifStatement	TiApiClient TiAutoUpdate TiClientData
syn keyword mifStatement	TiClientName TiClientSource TiClientType
syn keyword mifStatement	TiEOLisEOP TiFlow TiFlowName
syn keyword mifStatement	TiFormatRemoveOverrides
syn keyword mifStatement	TiFormatRemovePageBreaks TiFormatting
syn keyword mifStatement	TiImportHint TiLastUpdate TiMacEditionId
syn keyword mifStatement	TiMainFlow TiName TiPageSpace TiSrcFile
syn keyword mifStatement	TiTblHeadersEmpty TiTblIsByRow TiTblNumCols
syn keyword mifStatement	TiTblNumHdrRows TiTblNumSep TiTblSep TiTblTag
syn keyword mifStatement	TiTblTxtEncoding TiText TiTextTable
syn keyword mifStatement	TiTxtEncoding TipAngle Unconditional Unique
syn keyword mifStatement	UnitSymbol Units UserString Value Variable
syn keyword mifStatement	VariableDef VariableFormat VariableFormats
syn keyword mifStatement	VariableLocked VariableName Verbose View
syn keyword mifStatement	ViewCutout ViewInvisible ViewNumber Views
syn keyword mifStatement	VolNumComputeMethod VolumeNumStart
syn keyword mifStatement	VolumeNumStyle VolumeNumText XRef XRefDef
syn keyword mifStatement	XRefEnd XRefFormat XRefFormats XRefLastUpdate
syn keyword mifStatement	XRefLocked XRefName XRefSrcFile XRefSrcIsElem
syn keyword mifStatement	XRefSrcText

" Keywords
"TODO - list is incomplete
syn keyword mifKeyword         Anywhere Arabic AsFilename Below BodyPage Bottom
syn keyword mifKeyword         Butt CShown CUnderline CUpt Cent Center
syn keyword mifKeyword         ColorIsBlack ColorIsBlue ColorIsCyan
syn keyword mifKeyword         ColorIsGreen ColorIsMagenta ColorIsRed
syn keyword mifKeyword         ColorIsReserved ColorIsWhite ColorIsYellow
syn keyword mifKeyword         ColumnTop Contour DeleteEmptyPages EmDash EnDash
syn keyword mifKeyword         FAsTyped FNBaseline FNSuperscript FNoUnderlining
syn keyword mifKeyword         FNormal FSingle FirstRight Fixed Floating
syn keyword mifKeyword         GotoBehavior InHeader Inline Japanese Left
syn keyword mifKeyword         LeftMasterPage LeftOfCol LeftRight
syn keyword mifKeyword         MakePageCountEven Middle Narrow No NoLanguage
syn keyword mifKeyword         None Normal NotAnchored OtherMasterPage Outside
syn keyword mifKeyword         Page PageTop PerPage Portrait Proportional
syn keyword mifKeyword         ReadFromFile ReferencePage Restart Right
syn keyword mifKeyword         RightMasterPage RunIntoParagraph SoftHyphen
syn keyword mifKeyword         Solid Square StartNumbering StraddleNormalOnly
syn keyword mifKeyword         TOC Tab Top UKEnglish USEnglish Umm Variable Yes

" Units
syn keyword mifUnit             cc centimeter cicero cm dd didot in millimeter
syn keyword mifUnit             mm pc pica point pt 
"TODO - inch as double quote

" Integer number, or floating point number without a dot.
syn match   mifNumber           "\<\d\+\>"
" Floating point number, with dot
syn match   mifNumber           "\<\d\+\.\d*\>"
" Floating point number, starting with a dot
syn match   mifNumber           "\.\d\+\>"

" String and Character contstants
syn region  mifString           start="`" end="'"
syn region  mifComment          start="#" end="$"

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_mif_syntax_inits")
  if version < 508
    let did_mif_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink mifKeyword             Identifier
  HiLink mifNumber              Number
  HiLink mifPercentage          Number
  HiLink mifStatement           Statement
  HiLink mifString              String
  HiLink mifComment             Comment
  HiLink mifUnit                Special

  delcommand HiLink
endif

let b:current_syntax = "mif"

" vim: ts=8

#!/usr/bin/perl
# $Id$
use strict;
use warnings;
use FindBin qw($Bin);
use Data::Dump qw(dump);

my $outPutFormat;
if (defined $ARGV[0] && $ARGV[0] =~ /perl/i) {
  $outPutFormat = 'perl';
} else {
  $outPutFormat = 'kind-of-mif';
}

my (%newnod, %newlea, %newmul, %newatt);

my $nod = '';
open FH, "$Bin/mifstatementlist.txt" or die $!;
while (<FH>) {

  if ( /<(\S+)$/ ) {
    $nod = $1;
  }
  if ( /<(\S+) (.*)>/ ) {
    push @{$newnod{$nod}}, $1 unless grep {$1 eq $_} @{$newnod{$nod}};
    $newlea{$1} = $2 unless $2 eq '...';
    $newatt{$2}++ unless $2 eq '...';
    my $one = $1;
    $newmul{$one}++ if /\+/;
  }
  if ( />.*end of (\S+)/i ) {
    die "$nod $1 $." unless $nod eq $1;
    my $one = $1;
    $newmul{$one}++ if /\+/;
    $nod = '';
  }

}
close FH;

# for VIM syntax highlighting
#use Text::Wrap;
#$Text::Wrap::columns = 80;
#my $prepend = 'syn keyword mifStatement        ';
#print wrap($prepend, $prepend, sort(grep {/^[a-z0-9]/i} keys %newnod, keys %newlea));
#exit;


if ( $outPutFormat eq 'kind-of-mif' ) {

  for my $k (sort keys %newnod) {
    print "<$k\n" if $k;
    for (sort @{$newnod{$k}}) {
      my $space = $k ? ' ' : '';
      print "$space<$_ $newlea{$_}>" if exists $newlea{$_};
      print "$space<$_ ...>" unless exists $newlea{$_};
      print '+' if exists $newmul{$_};
      print "\n";
    }
    print "> # End of $k statement\n" if $k;
    print "\n";
  }

}

if ( $outPutFormat eq 'perl' ) {

  my @newatt = sort keys %newatt;
  my @newmul = sort keys %newmul;

  print <<COMMENTS;
package FrameMaker::MifTree;
# \044Id\044
###############################################################
#                                                             #
#               valid tags for MIF version 7.00               #
#                 part of package MifTree.pm                  #
#                                                             #
############ possibly incomplete and/or incorrect #############
use strict;
use warnings;
no warnings 'once';

# \%mifnodes defines all the nodes and their possible daughters
COMMENTS

  my $lines = dump(\%newnod);
  $lines =~ s/"(\S+)"/$1  /g;
  $lines =~ s/  ,/,/g;
  $lines =~ s/(\S)  ]/$1]/g;
  $lines =~ s/(\w),/$1/g;
  $lines =~ s/\[/[qw(/g;
  $lines =~ s/]/)]/g;
  $lines =~ s/{/\%FrameMaker::MifTree::mifnodes = (/;
  chomp $lines;
  substr($lines, -1) = ');';
  print $lines;

  print <<COMMENTS;


# \%mifleaves lists the valid leaves
# a leaf (in MIF context) is defined as an object that
# can't have daughters, but can have attributes
COMMENTS

  $lines = dump(\%newlea);
  $lines =~ s/(?<!> )"(\S+)"/$1  /g;
  $lines =~ s/"/'/g;
  $lines =~ s/{/\%FrameMaker::MifTree::mifleaves = (/;
  chomp $lines;
  substr($lines, -1) = ');';
  print $lines;

  print <<'COMMENTS';


# Attribute types
my $num = '-?\d+\.?\d*';
my $dim = $num .
'\s*(?:pt|point|"|in|mm|millimeter|cm|centimeter|pc|pica|dd|didot|cc|cicero)?';
%FrameMaker::MifTree::attribute_types = (
  '0xnnn'              => qr/^0x[0-9a-f]{3}\s*$/i,
  ID                   => qr/^\d+\s*$/,
  L_T_R_B              => qr/^$dim\s+$dim\s+$dim\s+$dim\s*$/o,
  L_T_W_H              => qr/^$dim\s+$dim\s+$dim\s+$dim\s*$/o,
  W_H                  => qr/^$dim\s+$dim\s*$/o,
  W_W                  => qr/^$dim\s+$dim\s*$/o,
  X_Y                  => qr/^$dim\s+$dim\s*$/o,
  X_Y_W_H              => qr/^$num\s+$num\s+$num\s+$num\s*$/o,
  boolean              => qr/^Yes|No\s*$/,
  data                 => qr/.*/,
  degrees              => qr/^$num\s*$/o,
  dimension            => qr/^$dim\s*$/o,
  empty                => qr/^\s*$/,
  integer              => qr/^-?\d+\s*$/,
  keyword              => qr/^\w+\s*$/,
  number               => qr/^$num\s*$/o,
  pathname             => qr/^`[^']*'\s*$/,
  percentage           => qr/^$num%?\s*$/o,
  seconds_microseconds => qr/^\d+\s+\d+\s*$/,
  string               => qr/^`[^']*'\s*$/,
  tagstring            => qr/^`[^']*'\s*$/,
);
COMMENTS

  print "\n1;\n\n=unused\n\n";

  $lines = dump(@newatt);
  $lines =~ s/\(/\%FrameMaker::MifTree::attribute_types = (/;
  $lines =~ s/,/ => '',/g;
  $lines =~ s/"(?!0)//g;
  $lines =~ s/\)/);/;
  print $lines, "\n\n";


  $lines = dump(@newmul);
  $lines =~ s/\(/\@allow_multiple = qw(/;
  $lines =~ s/,//g;
  $lines =~ s/"//g;
  $lines =~ s/\)/);/;
  print $lines;

  print "\n\n=cut\n";
}
